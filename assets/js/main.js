jQuery(document).ready(function( $ ) {

  // Back to top button
  $(window).scroll(function() {
    if ($(this).scrollTop() > 100) {
      $('.back-to-top').fadeIn('slow');
    } else {
      $('.back-to-top').fadeOut('slow');
    }
  });
  $('.back-to-top').click(function(){
    $('html, body').animate({scrollTop : 0},1500, 'easeInOutExpo');
    return false;
  });

  // Stick the header at top on scroll
  $("#header").sticky({topSpacing:0, zIndex: '50'});

  // Intro background carousel
  $("#intro-carousel").owlCarousel({
    autoplay: true,
    dots: false,
    loop: true,
    animateOut: 'fadeOut',
    items: 1
  });

  // Initiate the wowjs animation library
  new WOW().init();

  // Initiate superfish on nav menu
  $('.nav-menu').superfish({
    animation: {
      opacity: 'show'
    },
    speed: 400
  });

  // Mobile Navigation
  if ($('#nav-menu-container').length) {
    var $mobile_nav = $('#nav-menu-container').clone().prop({
      id: 'mobile-nav'
    });
    $mobile_nav.find('> ul').attr({
      'class': '',
      'id': ''
    });
    $('body').append($mobile_nav);
    $('body').prepend('<button type="button" id="mobile-nav-toggle"><i class="fa fa-bars"></i></button>');
    $('body').append('<div id="mobile-body-overly"></div>');
    $('#mobile-nav').find('.menu-has-children').prepend('<i class="fa fa-chevron-down"></i>');

    $(document).on('click', '.menu-has-children a', function(e) {
      $(this).toggleClass('menu-item-active');
      $(this).nextAll('ul').eq(0).slideToggle();
      $('.menu-has-children i').toggleClass("fa-chevron-up fa-chevron-down");
    });

    $(document).on('click', '#mobile-nav-toggle', function(e) {
      $('body').toggleClass('mobile-nav-active');
      $('#mobile-nav-toggle i').toggleClass('fa-times fa-bars');
      $('#mobile-body-overly').toggle();
    });

    $(document).click(function(e) {
      var container = $("#mobile-nav, #mobile-nav-toggle");
      if (!container.is(e.target) && container.has(e.target).length === 0) {
        if ($('body').hasClass('mobile-nav-active')) {
          $('body').removeClass('mobile-nav-active');
          $('#mobile-nav-toggle i').toggleClass('fa-times fa-bars');
          $('#mobile-body-overly').fadeOut();
        }
      }
    });
  } else if ($("#mobile-nav, #mobile-nav-toggle").length) {
    $("#mobile-nav, #mobile-nav-toggle").hide();
  }

  // Smooth scroll for the menu and links with .scrollto classes
  $('.nav-menu a, #mobile-nav a, .scrollto').on('click', function() {
    if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
      var target = $(this.hash);
      if (target.length) {
        var top_space = 0;

        if ($('#header').length) {
          top_space = $('#header').outerHeight();

          if( ! $('#header').hasClass('header-fixed') ) {
            top_space = top_space - 20;
          }
        }

        $('html, body').animate({
          scrollTop: target.offset().top - top_space
        }, 1500, 'easeInOutExpo');

        if ($(this).parents('.nav-menu').length) {
          $('.nav-menu .menu-active').removeClass('menu-active');
          $(this).closest('li').addClass('menu-active');
        }

        if ($('body').hasClass('mobile-nav-active')) {
          $('body').removeClass('mobile-nav-active');
          $('#mobile-nav-toggle i').toggleClass('fa-times fa-bars');
          $('#mobile-body-overly').fadeOut();
        }
        return false;
      }
    }
  });


  // Porfolio - uses the magnific popup jQuery plugin
  $('.portfolio-popup').magnificPopup({
    type: 'image',
    removalDelay: 300,
    mainClass: 'mfp-fade',
    gallery: {
      enabled: true
    },
    zoom: {
      enabled: true,
      duration: 300,
      easing: 'ease-in-out',
      opener: function(openerElement) {
        return openerElement.is('img') ? openerElement : openerElement.find('img');
      }
    }
  });

  // Testimonials carousel (uses the Owl Carousel library)
  $(".testimonials-carousel").owlCarousel({
    autoplay: true,
    dots: true,
    loop: true,
    responsive: { 0: { items: 1 }, 768: { items: 2 }, 900: { items: 3 } }
  });

  // Clients carousel (uses the Owl Carousel library)
  $(".clients-carousel").owlCarousel({
    autoplay: true,
    dots: true,
    loop: true,
    responsive: { 0: { items: 2 }, 768: { items: 4 }, 900: { items: 6 }
    }
  });

  //Google Map
  var get_latitude = $('#google-map').data('latitude');
  var get_longitude = $('#google-map').data('longitude');

  function initialize_google_map() {
    var myLatlng = new google.maps.LatLng(get_latitude, get_longitude);
    var mapOptions = {
      zoom: 14,
      scrollwheel: false,
      center: myLatlng
    };
    var map = new google.maps.Map(document.getElementById('google-map'), mapOptions);
    var marker = new google.maps.Marker({
      position: myLatlng,
      map: map
    });
  }
  //google.maps.event.addDomListener(window, 'load', initialize_google_map);
  // Smoth scroll
  $('#personal-info-btn').click(function(evt) {
    $('html, body').stop().animate({
      scrollTop: $("#personal-info").offset().top
    }, 1000);
  });

  $("#account-info-btn").click(function() {
    $('html, body').animate({
        scrollTop: $("#account-info").offset().top
    }, 2000);
  });

  $("#credentials-btn").click(function() {
    $('html, body').animate({
        scrollTop: $("#credentials").offset().top
    }, 2000);
  });


  // Modal jQuery

  /*$("#submit-verify-btn").click(function() {
    $("#verify-respond").modal({
      escapeClose: false,
      clickClose: false,
      showClose: false,
      fadeDuration: 100
    });
  });*/

  $("#complete-profile-btn").click(function() {
    $("#complete-profile").modal({
      escapeClose: false,
      clickClose: false,
      showClose: false,
      fadeDuration: 100
    });
  });

  $("#proceed-to-settings").click(function() {
    $(location).attr('href', 'settings')
  });

  //Toogle Language

  $(".switch-input").click(function() {
    var isChecked = $("input[name='lang-selection']:checked").val();

    if(isChecked == 'bm') {
      window.location.replace("?lang=bm");
    } else {
      window.location.replace("?lang=en");
    }
  });

// Carousel
  $('.slide-career').slick({
    dots: false,
    infinite: true,
    speed: 600,
    slidesToShow: 3,
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1,
          infinite: true,
          dots: true
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
      // You can unslick at a given breakpoint now by adding:
      // settings: "unslick"
      // instead of a settings object
    ]
  });

  // Validation Login & SignUp
  $("#submit-login").click(function() {
    var getEmail = jQuery('#email-login').val();
    var getPass = jQuery('#pass-login').val();

    if ( getEmail == '' && getPass == '' ) {
      jQuery('#error-email-login').show();
      jQuery('#error-pass-login').show();
      jQuery('#email-login, #pass-login').css('border','1px solid red');
      allvalid = false;
      return false;
    }

    if ( getEmail == '' ) {
      jQuery('#error-email-login').show();
      jQuery('#email-login').css('border','1px solid red');
      return false;
    }

    if ( !validEmail(getEmail) ) {
      jQuery('#error-email-login-pattern').show();
      jQuery('#email-login').css('border','1px solid red');
      return false;
    }

    if ( getPass == '' ) {
      jQuery('#error-pass-login').show();
      jQuery('#pass-login').css('border','1px solid red');
      return false;
    }

    function validEmail(email) {
      var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
      return regex.test(email);
    }

  });

   // Validation Login & SignUp
   $("#submit-signup").click(function() {
    var getEmail = jQuery('#email-login').val();
    var getPass = jQuery('#pass-login').val();
    var getFullname = jQuery('#fullname').val();

    if ( getEmail == '' && getPass == '' && getFullname == '') {
      jQuery('#error-email-login').show();
      jQuery('#error-pass-login').show();
      jQuery('#error-fullname-signup').show();
      jQuery('#email-login, #pass-login, #fullname').css('border','1px solid red');
      allvalid = false;
      return false;
    }

    if ( getEmail == '' ) {
      jQuery('#error-email-login').show();
      jQuery('#email-login').css('border','1px solid red');
      return false;
    }

    if ( !validEmail(getEmail) ) {
      jQuery('#error-email-login-pattern').show();
      jQuery('#email-login').css('border','1px solid red');
      return false;
    }

    if ( getPass == '' ) {
      jQuery('#error-pass-login').show();
      jQuery('#pass-login').css('border','1px solid red');
      return false;
    }

    if ( getFullname == '' ) {
      jQuery('#error-fullname-signup').show();
      jQuery('#fullname').css('border','1px solid red');
      return false;
    }

    function validEmail(email) {
      var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
      return regex.test(email);
    }

  });

  // Validation SignUp Extra
  $("#submit-extra").click(function() {
    var getPhone = jQuery('#extra-phone').val();

    if ( getPhone == '' ) {
      jQuery('#error-extra-phone').show();
      jQuery('#extra-phone').css('border','1px solid red');
      return false;
    }

  });

  $("#submit-verify-btn").click(function() {
    var getEmail = jQuery('#inst-email').val();
    var getName = jQuery('#inst-name').val();

    if ( getName == '' && getEmail == '' ) {
      jQuery('#inst-error-name, #inst-error-email').show();
      jQuery('#inst-name, #inst-email').css('border','1px solid red');
      return false;
    }

    if ( getName == '' ) {
      jQuery('#inst-error-name').show();
      jQuery('#inst-name').css('border','1px solid red');
      return false;
    }

    if ( getEmail == '' ) {
      jQuery('#inst-error-email').show();
      jQuery('#inst-email').css('border','1px solid red');
      return false;
    }

    if ( !validEmail(getEmail) ) {
      jQuery('#inst-error-email-pattern').show();
      jQuery('#inst-email').css('border','1px solid red');
      return false;
    }

    function validEmail(email) {
      var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
      return regex.test(email);
    }

  });

  $("#forgot-pass-btn").click(function() {
    var getEmail = jQuery('#forgot-pass-email').val();

    if ( getEmail == '' ) {
      jQuery('#error-email-login').show();
      jQuery('#forgot-pass-email').css('border','1px solid red');
      return false;
    }

    if ( !validEmail(getEmail) ) {
      jQuery('#error-email-login-pattern').show();
      jQuery('#forgot-pass-email').css('border','1px solid red');
      return false;
    }

    function validEmail(email) {
      var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
      return regex.test(email);
    }

  });

  $('#month').on('change', function() {
    var getMonth = $('#month').val();
    var getYear = $('#year').val();
    if ( getMonth != '' && getYear != '' ) {
      $(location).attr('href', '?m='+getMonth+'&y='+getYear);
    } else {
      $(location).attr('href', '?m=all');
    }
    
  });

  $('#year').on('change', function() {
    var getMonth = $('#month').val();
    var getYear = $('#year').val();
    if ( getMonth != '' && getYear != '' ) {
      $(location).attr('href', '?m='+getMonth+'&y='+getYear);
    } else {
      $(location).attr('href', '?m=all');
    }
    
  });

  var isMobile = false; //initiate as false
// device detection
if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent) 
    || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0,4))) { 
    isMobile = true;
}

if ( isMobile ) {
  $('.desktop').hide();
  $('.mobile').show();
} else {
  $('.desktop').show();
  $('.mobile').hide();
}

  
});
