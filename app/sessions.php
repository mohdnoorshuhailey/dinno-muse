<?php
session_start();

if (isset($_SESSION['LAST_ACTIVITY']) && (time() - $_SESSION['LAST_ACTIVITY'] > 3600)) {
    session_unset();    
    session_destroy();
}

// Language
$defaultLang = 'en';
if (!empty($_GET["lang"])) { 
    switch (strtolower($_GET["lang"])) {
        case "en":
            $_SESSION['lang'] = 'en';
            break;
        case "bm":
            $_SESSION['lang'] = 'bm';
            break;
        default:
            $_SESSION['lang'] = $defaultLang;
            break;
    }
}

if (empty($_SESSION["lang"])) {
    $_SESSION["lang"] = $defaultLang;
}

if( !isset($_SESSION["allowedlogin"]) OR $_SESSION["allowedlogin"] == FALSE ) {
    $_SESSION["allowedlogin"] = FALSE;
}

require_once 'languages/'.$_SESSION['lang'].'.php';
require_once 'config.php';

$currentPage = $_SERVER['PHP_SELF'];
$currentPage = explode('/',$currentPage);
$currentPage = end($currentPage);
$currentPage = str_replace(".php","",$currentPage);

$restrictSession = FALSE;
$currentTitle = $lang['home'];

// Assign Title for each pages
foreach (LIST_PAGES as $listpage) {
    if ( $currentPage == $listpage ) {
        $currentTitle = $lang[$listpage];
    }
}


foreach (EXCLUDE_SESSION_PAGE as $page) {
    if ( $currentPage == $page ) {
        $restrictSession = TRUE;
    }
}

if ( $restrictSession === TRUE AND $_SESSION["allowedlogin"] === TRUE ) {

    $allowedlogin = $_SESSION["allowedlogin"];
    $loginsession = 0;

    if ($allowedlogin === TRUE AND $restrictSession == FALSE) {
        $_SESSION["login"] = true;
        $loginsession = 1;
    } else if ($allowedlogin === TRUE AND $restrictSession == TRUE) {
        header("Location: settings");
        exit;
    } else {
        $_SESSION["login"] = false;
        $loginsession = 0;
        header("Location: login");
        exit;
    }

} else {
    if ( $currentPage == 'settings' AND $_SESSION["allowedlogin"] === NULL ) {
        $_SESSION["login"] = false;
        $loginsession = 0;
        header("Location: login");
        exit;
    }
}

// Userdata
if ( isset($_SESSION["userdata"]) ) {
    $userdata = $_SESSION["userdata"];
} else {
    $userdata = '';
}

// to get current url
function url_origin( $s, $use_forwarded_host = false )
{
    $ssl      = ( ! empty( $s['HTTPS'] ) && $s['HTTPS'] == 'on' );
    $sp       = strtolower( $s['SERVER_PROTOCOL'] );
    $protocol = substr( $sp, 0, strpos( $sp, '/' ) ) . ( ( $ssl ) ? 's' : '' );
    $port     = $s['SERVER_PORT'];
    $port     = ( ( ! $ssl && $port=='80' ) || ( $ssl && $port=='443' ) ) ? '' : ':'.$port;
    $host     = ( $use_forwarded_host && isset( $s['HTTP_X_FORWARDED_HOST'] ) ) ? $s['HTTP_X_FORWARDED_HOST'] : ( isset( $s['HTTP_HOST'] ) ? $s['HTTP_HOST'] : null );
    $host     = isset( $host ) ? $host : $s['SERVER_NAME'] . $port;
    return $protocol . '://' . $host;
}

function full_url( $s, $use_forwarded_host = false )
{
    return url_origin( $s, $use_forwarded_host ) . $s['REQUEST_URI'];
}

$absolute_url = full_url( $_SERVER );

// News Session
if ( isset($_SESSION["newsidsession"]) AND $_SESSION["newsidsession"] !== NULL AND $currentPage === 'news-details' ) {
    //$_SESSION["newsidsession"]
} else if ( $currentPage === 'news-details' AND isset($_REQUEST['nid']) ) {
    $_SESSION["newsidsession"] = $_REQUEST['nid'];
} else {
    $_SESSION["newsidsession"] = NULL;
}
