<?php 

// Define Environment
define ('MODE_ENV', 'Development'); // Production // Development
define ('ADMIN_EMAIL', 'admin@dinno.com.my'); 

// Define base url
define ('BASE_URL', 'http://mock.dinnoportal.com/prestariang/student/'); // Update this
define ('UPLOADS_URL', 'http://mock.dinnoportal.com/prestariang/uploads/'); // Update this

// Database connection
define ('DB_HOSTNAME', 'localhost');
define ('DB_DATABASE', 'dinnopor_musedb');
define ('DB_USERNAME', 'dinnopor_muse');
define ('DB_PASSWORD', '1KAq]N4))yI2');

//Skip All Warning
error_reporting(E_ERROR | E_PARSE);

// Exclude files if login
$listExcludePage = array(
    'login',
    'signup',
    'signup-extra',
    'verify-account',
    'forgot-password'
);

define('EXCLUDE_SESSION_PAGE', $listExcludePage);

$listPages = array(
    'home',
    'office365',
    'imagine',
    'imagine-academy',
    'windows10',
    'news-events',
    'login',
    'signup',
    'verify-account',
    'forgot-password',
    'settings',
    'adobe',
    'autodesk'
);

define('LIST_PAGES', $listPages);

// Get Days
$listDays = array(
    '1' => $lang['Monday'],
    '2' => $lang['Tuesday'],
    '3' => $lang['Wednesday'],
    '4' => $lang['Thursday'],
    '5' => $lang['Friday'],
    '6' => $lang['Saturday'],
    '7' => $lang['Sunday'],
);

define('LIST_DAYS', $listDays);

?>