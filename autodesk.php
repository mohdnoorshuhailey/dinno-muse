<?php
include 'header.php';
?>
  <section id="introautodesk">
    <div class="container">
      <div class="content">
        <h2><?php echo $lang['Autodesk']; ?></h2>
        <p class="intro-txt"><?php echo $lang['subhead-autodesk']; ?></p>
      </div>
    </div>
  </section><!-- #intro -->

<?php
include 'footer.php';
?>