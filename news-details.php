<?php
include 'header.php';
include 'app/pagescontroller.php';
?>
  
  <main id="main">
    <section id="newsdetails">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <?php 
            $dataNews = getNewsDetails($_SESSION["newsidsession"]); 
            while ( $dataNews = getObject($dataNews) ) {
              if ( $_SESSION["lang"] == 'bm' ) {
                $news_title = $dataNews->news_title_bm;
                $news_desc  = $dataNews->news_description_bm;
              } else {
                $news_title = $dataNews->news_title_en;
                $news_desc  = $dataNews->news_description_en;
              }
              
            ?>
              <div class="newsdetailsmainbox">
                <div class="breadcrumb-news"><a href="news-events"><?php echo $lang['newsevents'];?></a> <i class="fa fa-angle-right"></i> <?php echo $news_title; ?></div>
                <img src="<?php echo UPLOADS_URL; ?>news/<?php echo $dataNews->news_image; ?>" style="width:100%" alt="" />
                <div class="newsdetails-title"><h3><?php echo $news_title; ?></h3></div>
                <div class="newsdetails-date"><?php echo date("d.m.Y",strtotime($dataNews->news_date_created)); ?></div>
                <div class="newsdetails-desc"><?php echo nl2br($news_desc); ?></div>
              </div>
            <?php  
            }
            ?>
          </div>
        </div>
      </div>
    </section>
    

  </main>

<?php
include 'footer.php';
?>