<?php
include 'header.php';
?>
  <section id="intro-office365">
    <div class="container">
      <div class="content">
        <h2><?php echo $lang['Microsoft Office 365']; ?></h2>
        <p class="intro-txt"><?php echo $lang['subhead-office365']; ?></p>
      </div>
    </div>
  </section><!-- #intro -->

  <main id="main">
  <section id="office365info1">
    <div class="container">
      <div class="row office365column">
        <div class="col-lg-6">
          <img class="img-left" src="assets/img/office365-img1.jpg" alt="" />
        </div>
        <div class="col-lg-6">
          <h3><?php echo $lang['Microsoft Office 365 for Education']; ?></h3>
          <ul class="office365-products">
            <li><img src="assets/img/office365-icon1.png" alt="" style="height:58px"/></li>
            <li><img src="assets/img/office365-icon2.jpg" alt="" /></li>
            <li><img src="assets/img/office365-icon3.jpg" alt="" /></li>
            <li><img src="assets/img/office365-icon4.jpg" alt="" /></li>
            <li><img src="assets/img/office365-icon5.jpg" alt="" /></li>
            <li><img src="assets/img/office365-icon6.jpg" alt="" /></li>
            <li><img src="assets/img/office365-icon7.jpg" alt="" /></li>
            <li><img src="assets/img/office365-icon8.jpg" alt="" /></li>
            <li><img src="assets/img/office365-icon9.jpg" alt="" /></li>
          </ul>
          <br clear="both">
          <p class="normal-txt"><?php echo $lang['office365-edu']; ?></p>
          <div>
          <?php if ( $_SESSION["userdata"]['schoolEmail'] == '' ) { ?>
            <a href="settings" class="btn-office365"><?php echo $lang['CLAIM YOURS!']; ?></a>
          <?php } else if ( $_SESSION["userdata"]['isSchoolEmailActivated'] == false ) { ?>
            <a href="settings" class="btn-office365"><?php echo $lang['CLAIM YOURS!']; ?></a>
          <?php } else { ?>
            <a href="https://products.office.com/en-my/student/office-in-education" class="btn-office365" target="_blank"><?php echo $lang['CLAIM YOURS!']; ?></a>
          <?php } ?>
          </div>
        </div>
      </div>

      <div class="row office365column">
        <div class="col-lg-6">
          <img class="img-left" src="assets/img/office365-img2.jpg" alt="" />
        </div>
        <div class="col-lg-6">
          <h3><?php echo $lang['Microsoft Office 365 ProPlus']; ?></h3>
          <ul class="office365-products">
            <li><img src="assets/img/office365-icon1.png" style="height:58px" alt="" /></li>
            <li><img src="assets/img/office365-icon2.jpg" alt="" /></li>
            <li><img src="assets/img/office365-icon3.jpg" alt="" /></li>
            <li><img src="assets/img/office365-icon4.jpg" alt="" /></li>
            <li><img src="assets/img/office365-icon5.jpg" alt="" /></li>
            <li><img src="assets/img/office365-icon10.jpg" alt="" /></li>
            <li><img src="assets/img/office365-icon11.jpg" alt="" /></li>
            <li><img src="assets/img/office365-icon12.jpg" alt="" /></li>
            <li><img src="assets/img/office365-icon13.jpg" alt="" /></li>
          </ul>
          <br clear="both">
          <p class="normal-txt"><?php echo $lang['office365-proplus']; ?></p>
          <div>
          <?php if ( $_SESSION["userdata"]['schoolEmail'] == '' ) { ?>
            <a href="settings" class="btn-office365"><?php echo $lang['CLAIM YOURS!']; ?></a>
          <?php } else if ( $_SESSION["userdata"]['isSchoolEmailActivated'] == false ) { ?>
            <a href="settings" class="btn-office365"><?php echo $lang['CLAIM YOURS!']; ?></a>
          <?php } else { ?>
            <a href="https://products.office.com/en-my/business/office-365-proplus-business-software" class="btn-office365" target="_blank"><?php echo $lang['CLAIM YOURS!']; ?></a>
          <?php } ?>
          </div>
        </div>
      </div>
    </div>
  </section><!-- #howtoredeem -->

  <section id="knowmore">
    <div class="container">
      <h1><?php echo $lang['knowmore']; ?></h1>
      <p><?php echo $lang['knowmore-txt']; ?></p>
      <div>
          <a href="institution-list" class="form-submit-button scrollto" style="padding: 15px 75px;background:#F2BF02;"><?php echo $lang['ASK NOW!']; ?></a>
      </div>
    </div>
  </section><!-- #howtoredeem -->

  </main>

<?php
include 'footer.php';
?>