<?php
include 'header.php';
?>
  <section id="intro-home">
    <div class="container">
      <div class="content">
        <h2><?php echo $lang['home-title']; ?></h2>
        <p class="intro-txt"><?php echo $lang['subhead-home']; ?></p>
        <div>
          <a href="signup" class="intro-submit-button scrollto" style="padding: 15px 75px;"><?php echo $lang['Join Now!']; ?></a>
        </div>
      </div>
    </div>
  </section><!-- #intro -->
  <main id="main">
  <section id="ourpartner">
    <div class="container contaner-extend">
      <div class="row">
        <ul class="ourpartner-list">
          <li class="mobile-align"><h6><?php echo $lang['ourpartner']; ?></h6></li>
          <li><img src="assets/img/homepage/home_partner1.jpg" width="75" alt="" /></li>
          <li><img src="assets/img/homepage/home_partner2.jpg" width="95" alt="" /></li>
          <li><img src="assets/img/homepage/home_partner3.jpg" width="75" alt="" /></li>
          <li><img src="assets/img/homepage/home_partner4.jpg" width="130" alt="" /></li>
          <li><img src="assets/img/homepage/home_partner5.jpg" width="50" alt="" /></li>
          <li><img src="assets/img/homepage/home_partner6.jpg" width="140" alt="" /></li>
          <li><img src="assets/img/homepage/home_partner7.jpg" width="120" alt="" /></li>
          <li><img src="assets/img/homepage/home_partner8.jpg" width="95" alt="" /></li>
        </ul>
      </div>
    </div>
  </section>

  <section id="getwithmuse">
    <div class="container">
    <h3><?php echo $lang['getwithmusetitle']; ?></h3>
      <div class="row getwithmuse">
        <div class="col-lg-4">
          <img src="assets/img/homepage/home_getimg1.jpg" width="280" alt="" />
          <h5><?php echo $lang['Free Software']; ?></h5>
          <p class="normal-txt"><?php echo $lang['freesoftware-txt']; ?></p> 
        </div>
        <div class="col-lg-4">
          <img src="assets/img/homepage/home_getimg2.jpg" width="280" alt="" />
          <h5><?php echo $lang['Learn for free']; ?></h5>
          <p class="normal-txt"><?php echo $lang['learnfree-txt']; ?></p> 
        </div>
        <div class="col-lg-4">
          <img src="assets/img/homepage/home_getimg3.jpg" width="280" alt="" />
          <h5><?php echo $lang['Explore Career']; ?></h5>
          <p class="normal-txt"><?php echo $lang['career-txt']; ?></p> 
        </div>
      </div>
    </div>
  </section>

  <section id="homepage-column1">
    <div class="container">
      <div class="row">
        <div class="col-lg-6">
          <img class="homepage-img" src="assets/img/homepage/home_imgcolumn1.jpg" width="450" alt="" />
        </div>
        <div class="col-lg-6 column-content">
          <h3><?php echo $lang['column1-title']; ?></h3>
          <p class="normal-txt"><?php echo $lang['column1-content']; ?></p> 
          <img class="separator" src="assets/img/homepage/home_separator.jpg" width="400" alt="" />
          <p class="normal-txt2"><?php echo $lang['column1-content2']; ?></p>
          <ul class="cloumn-list">
            <li><img src="assets/img/homepage/home_adobe.jpg" width="35" alt="" /></li>
            <li><img src="assets/img/homepage/home_office365.jpg" width="135" alt="" /></li>
            <li><img src="assets/img/homepage/microsoft-icon.png" width="135" alt="" /></li>
          </ul>
        </div>
      </div>
    </div>
  </section>

  <section id="homepage-column2" class="desktop">
    <div class="container">
      <div class="row">
        <div class="col-lg-6 column-content">
          <h3><?php echo $lang['column2-title']; ?></h3>
          <p class="normal-txt"><?php echo $lang['column2-content']; ?></p> 
          <img class="separator" src="assets/img/homepage/home_separator.jpg" width="400" alt="" />
          <p class="normal-txt2"><?php echo $lang['column2-content2']; ?></p>
          <ul class="cloumn-list">
            <li><img src="assets/img/homepage/microsoft-icon.png" style="margin-top:10px;margin-left:-25px;" width="135" alt="" /></li>
            <li><img src="assets/img/homepage/imagine-academy-icon.png" width="45" alt="" /></li>
          </ul>
        </div>
        <div class="col-lg-6">
          <img class="homepage-img" src="assets/img/homepage/home_imgcolumn2.jpg" width="450" alt="" />
        </div>
        
      </div>
    </div>
  </section>

  <section id="homepage-column2" class="mobile" style="display:none">
    <div class="container">
      <div class="row">
        <div class="col-lg-6">
          <img class="homepage-img" src="assets/img/homepage/home_imgcolumn2.jpg" width="450" alt="" />
        </div>
        <div class="col-lg-6 column-content">
          <h3><?php echo $lang['column2-title']; ?></h3>
          <p class="normal-txt"><?php echo $lang['column2-content']; ?></p> 
          <img class="separator" src="assets/img/homepage/home_separator.jpg" width="400" alt="" />
          <p class="normal-txt2"><?php echo $lang['column2-content2']; ?></p>
          <ul class="cloumn-list">
            <li><img src="assets/img/homepage/microsoft-icon.png" style="margin-top:10px;margin-left:-25px;" width="135" alt="" /></li>
            <li><img src="assets/img/homepage/imagine-academy-icon.png" width="45" alt="" /></li>
          </ul>
        </div> 
      </div>
    </div>
  </section>

  <section id="homepage-column3">
    <div class="container">
      <div class="row">
        <div class="col-lg-6">
          <img class="homepage-img" src="assets/img/homepage/home_imgcolumn3.jpg" width="450" alt="" />
        </div>
        <div class="col-lg-6 column-content">
          <h3><?php echo $lang['column3-title']; ?></h3>
          <p class="normal-txt"><?php echo $lang['column3-content']; ?></p> 
          <img class="separator" src="assets/img/homepage/home_separator.jpg" width="400" alt="" />
          <p class="normal-txt2"><?php echo $lang['column3-content2']; ?></p>
          <ul class="cloumn-list">
            <li><img style="padding-top:15px;margin-left:-42px;" src="assets/img/homepage/home_talent.jpg" width="150" alt="" /></li>
            <li><img src="assets/img/homepage/home_jobmatching.jpg" width="135" alt="" /></li>
          </ul>
        </div>
      </div>
    </div>
  </section>

  <section id="howtostart">
    <div class="container">
    <h3><?php echo $lang['howtostart']; ?></h3>
      <div class="row">
        <div class="col-lg-4">
          <img src="assets/img/homepage/How-to-redeem1.png" width="150" alt="" />
          <h3><?php echo $lang['registerwithmuse']; ?></h3>
        </div>
        <div class="col-lg-4">
          <img src="assets/img/homepage/How-to-redeem2.png" width="150" alt="" />
          <h3><?php echo $lang['verifystudentstatus']; ?></h3>
        </div>
        <div class="col-lg-4">
          <img src="assets/img/homepage/How-to-redeem3.png" width="150" alt="" />
          <h3><?php echo $lang['studentbenefit']; ?></h3>
        </div>
      </div>
    </div>
  </section><!-- #howtostart -->

  <section id="homeslider">
    <div class="container">
    <h3><?php echo $lang['homeslider-title']; ?></h3>
        <div class="slide-career">
          <div class="col-lg-4 tabContainer" style="margin: 0px 30px;">
            <img src="assets/img/homepage/home_slide1.jpg" width="180" alt="" />
            <h5><?php echo $lang['homeslider-title1']; ?></h5>
            <p class="small-txt"><?php echo $lang['homeslider-center1']; ?></p>
          </div>
          <div class="col-lg-4 tabContainer" style="margin: 0px 30px;">
            <img src="assets/img/homepage/home_slide2.jpg" width="120" alt="" />
            <h5><?php echo $lang['homeslider-title2']; ?></h5>
            <p class="small-txt"><?php echo $lang['homeslider-center2']; ?></p>
          </div>
          <div class="col-lg-4 tabContainer" style="margin: 0px 30px;">
            <img src="assets/img/homepage/home_slide1.jpg" width="180" alt="" />
            <h5><?php echo $lang['homeslider-title3']; ?></h5>
            <p class="small-txt"><?php echo $lang['homeslider-center3']; ?></p>
          </div>
          <div class="col-lg-4 tabContainer" style="margin: 0px 30px;">
            <img src="assets/img/homepage/home_slide2.jpg" width="120" alt="" />
            <h5><?php echo $lang['homeslider-title2']; ?></h5>
            <p class="small-txt"><?php echo $lang['homeslider-center2']; ?></p>
          </div>
      </div>
    </div>
  </section><!-- #homeslider -->

  <section id="allbenefit">
    <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <h2><?php echo $lang['allbenefit-title']; ?></h2>
            <div>
            <a href="signup" class="allbenefit-submit-button" style="padding: 15px 75px;"><?php echo $lang['GET STARTED!']; ?></a>
            </div>
          </div>
      </div>
    </div>
  </section><!-- #howtostart -->

  <section id="attention">
    <div class="container" style="max-width:1230px !important">
        <div class="row">
          <div class="col-lg-8">
            <h2><?php echo $lang['attention-title']; ?></h2>
            <ul class="attentionlist">
              <li><?php echo $lang['attention-list1']; ?></li>
              <li><?php echo $lang['attention-list2']; ?></li>
              <li><?php echo $lang['attention-list3']; ?></li>
              <li><?php echo $lang['attention-list4']; ?></li>
            </ul>
          </div>
          <div class="col-lg-4" style="text-align:center">
            <img src="assets/img/homepage/international-student-img.png" width="330" alt="" />
            <div>
            <a href="https://educationmalaysia.gov.my/" class="attention-submit-button" style="padding: 15px 28px;text-align:center" target="_blank"><?php echo $lang['CLICK HERE']; ?> <i class="fa fa-angle-right"></i></a>
            </div>
          </div>
      </div>
    </div>
  </section><!-- #attention -->

  </main>

<?php
include 'footer.php';
?>