<?php
include 'header.php';
?>
  <section id="introadobe">
    <div class="container">
      <div class="content">
        <h2><?php echo $lang['Adobe']; ?></h2>
        <p class="intro-txt"><?php echo $lang['subhead-adobe']; ?></p>
      </div>
    </div>
  </section><!-- #intro -->

  <main id="main">

  <section id="adobeproducts">
    <div class="container">
      <div class="row tabContainer">
        <div class="col-lg-4">
          <img src="assets/img/adobe_img1.png" width="230" alt="" />
          <h5><?php echo $lang['adobe-title1']; ?></h5>
        </div>
        <div class="col-lg-8 products-collection">
          <p><?php echo $lang['adobe-content1'];?></p>
        </div>
      </div>
      <div class="row tabContainer">
        <div class="col-lg-4">
          <img src="assets/img/adobe_img2.png" width="230" alt="" />
          <h5><?php echo $lang['adobe-title2']; ?></h5>
        </div>
        <div class="col-lg-8 products-collection">
          <p><?php echo $lang['adobe-content2'];?></p>
        </div>
      </div>
      <div class="row tabContainer">
        <div class="col-lg-4">
          <img src="assets/img/adobe_img3.png" width="230" alt="" />
          <h5><?php echo $lang['adobe-title3']; ?></h5>
        </div>
        <div class="col-lg-8 products-collection">
          <p><?php echo $lang['adobe-content3'];?></p>
        </div>
      </div>

    </div>
  </section><!-- #products -->

  </main>

<?php
include 'footer.php';
?>