<?php
include 'header.php';
include 'app/usercontroller.php';
?>
  
  <main id="main">

    <!--==========================
      SIGN UP Section
    ============================-->
    <section id="signup" class="wow">
      <div class="container">
        <div class="row">     
            <div class="tabBox">
              <h3><?php echo $lang['signup-header-txt']; ?>*<br><span style="font-size:10px;float:right;padding-right: 70px;"><?php echo $lang['tncapply']; ?></span></h3>
              <ul class="tabs">
                <li><a href="signup" style="border-right:0px;"><?php echo $lang['signup']; ?></a></li>
                <li class="dimbox"><a href="login" style="border-left:0px;"><?php echo $lang['login']; ?></a></li>
              </ul>
            
              <div class="tabContainer">
                <div id="signupform" class="tabContent">
                  <?php if ( $_SESSION["messageServer"] != '' ) { ?>
                    <p id="errMsg" class="error" style="margin-left: 10px;display:block"><?php echo $_SESSION["messageServer"]; ?></p>
                  <?php } ?>
                  <form name="signupform" action="" method="POST">
                    <input type="text" id="fullname" class="input" name="fullname" placeholder="<?php echo $lang['fullname']; ?>" required>
                    <p id="error-fullname-signup" class="error"><?php echo $lang['valid-fullname']; ?></p>
                    <input type="email" id="email-login" class="input" name="email" placeholder="<?php echo $lang['email']; ?>" required>
                    <p id="error-email-login" class="error"><?php echo $lang['valid-email']; ?></p>
                    <p id="error-email-login-pattern" class="error"><?php echo $lang['valid-email-pattern']; ?></p>
                    <input type="password" id="pass-login" class="input" name="password" placeholder="<?php echo $lang['password']; ?>" required>
                    <p id="error-pass-login" class="error"><?php echo $lang['valid-pass']; ?></p>
                    <?php if ( $_SESSION["messagePass"] != '' ) { ?>
                      <p id="errMsg" class="error" style="margin-left: 10px;display:block;width: 95%;"><?php echo $_SESSION["messagePass"]; ?></p>
                    <?php } ?>

                    <p class="form-txt-agree"><?php echo $lang['signup-agree-txt']; ?> <a href="" target="_blank"><?php echo $lang['Term of Services']; ?></a></p>
                    <input type="submit" id="submit-signup" class="form-submit-button" name="submit-signup" value="<?php echo $lang['signup']; ?>" />
                  </form>
                </div>   
              </div>
            </div> 
        </div>

      </div>
    </section><!-- #signup -->
    <div id="signup-respond" class="modal">
      <h3><?php echo $lang['welcomeverify']; ?>, <?php echo $_SESSION["userdata"]['fullName'];?> !</h3>
      <p><?php echo $lang['verifymsg']; ?></p>
      <a href="#" id="proceed-to-settings" class="button" rel="modal:close">OK</a>
    </div>

  </main>

<?php
include 'footer.php';
?>