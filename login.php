<?php
include 'header.php';
include 'app/usercontroller.php';
?>
  
  <main id="main">

    <!--==========================
      LOG IN Section
    ============================-->
    <section id="signup" class="wow">
      <div class="container">
        <div class="row">     
            <div class="tabBox">
              <ul class="tabs">
                <li class="dimbox"><a href="signup" style="border-right:0px;"><?php echo $lang['signup']; ?></a></li>
                <li><a href="login" style="border-left:0px;"><?php echo $lang['login']; ?></a></li>
              </ul>
              
              <div class="tabContainer">
                <div id="signupform" class="tabContent">
                  <form id="loginform" name="loginform" action="" method="POST">
                    <?php if ( $_SESSION["message"] != '' ) { ?>
                      <p id="errMsg" class="error" style="display:block"><?php echo $_SESSION["message"]; ?></p>
                    <?php } ?>
                    <?php if ( $_SESSION["error"] != '' ) { ?>
                      <p id="errMsg" class="error" style="display:block">Debug (hide on production): <?php echo $_SESSION["error"]; ?></p>
                    <?php } ?>
                    <input type="email" id="email-login" class="input" name="email" placeholder="<?php echo $lang['email']; ?>" required>
                    <p id="error-email-login" class="error"><?php echo $lang['valid-email']; ?></p>
                    <p id="error-email-login-pattern" class="error"><?php echo $lang['valid-email-pattern']; ?></p>
                    <input type="password" id="pass-login" class="input" name="password" placeholder="<?php echo $lang['password']; ?>" required>
                    <p id="error-pass-login" class="error"><?php echo $lang['valid-pass']; ?></p>
                    
                    <input type="submit" id="submit-login" class="form-submit-button" name="submit-login" value="<?php echo $lang['login']; ?>" >
                    <p class="form-txt-agree"><?php echo $lang['First time sign in']; ?>? <a href="signup.php"><?php echo $lang['Sign Up']; ?></a><br><a href="forgot-password"><?php echo $lang['Forgot Password']; ?></a></p>
                  </form>
                </div>   
              </div>
            </div> 
        </div>

      </div>
    </section><!-- #LOGIN -->

  </main>

<?php
include 'footer.php';
?>