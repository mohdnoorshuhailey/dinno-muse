<?php
include 'header.php';
include 'app/pagescontroller.php';
?>
  
  <main id="main">
    <section id="allevents" style="margin-top:3rem;">
        <div class="container">
        <h3><?php echo $lang['allnews'];?></h3>
            <?php 
            $dataEvents = getAllNews(); 
            while ( $dataAllEvents = getObject($dataEvents) ) {
              if ( $_SESSION["lang"] == 'bm' ) {
                $events_title = $dataAllEvents->news_title_bm;
                $events_desc  = $dataAllEvents->news_description_bm;
                
              } else {
                $events_title = $dataAllEvents->news_title_en;
                $events_desc  = $dataAllEvents->news_description_en;
              }
              
            ?>
              <div class="row alleventsbox">
                <div class="col-lg-3"><img class="allevent-img" src="app/timthumb.php?src=<?php echo UPLOADS_URL; ?>news/<?php echo $dataAllEvents->news_image; ?>&w=160&h=160" alt="" /></div>
                <div class="col-lg-7">
                  <div class="alleventsbox-info">
                    <div class="events-title"><h5><a href="news-details?nid=<?php echo $dataAllEvents->news_id; ?>"><?php echo $events_title; ?></a></h5></div>
                    <div class="events-desc"><?php echo substr($events_desc, 0, 600).' ...'; ?></div>
                  </div>
                </div>
                <div class="col-lg-2">
                  <div class="eventdate-box">
                    <div class="day">
                    <?php
                    $days = date("N",strtotime($dataAllEvents->news_date_created));
                    foreach ( LIST_DAYS as $day => $val ) {
                      if ( $day == $days ) {
                        echo $val;
                      }
                    } 
                    ?></div>
                    <div class="month-date">
                      <div class="month-date-m"><?php echo date("F",strtotime($dataAllEvents->news_date_created)); ?></div>
                      <div class="month-date-d"><?php echo date("d",strtotime($dataAllEvents->news_date_created)); ?></div>
                    </div>
                  </div>
                </div>
              </div>
            <?php  
            }
            ?>
      </div>
    </section>

  </main>

<?php
include 'footer.php';
?>