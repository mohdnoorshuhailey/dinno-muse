<?php
include 'header.php';
include 'app/usercontroller.php';
?>
  
  <main id="main">

    <!--==========================
      Forgot Password Section
    ============================-->
    <section id="signup" class="wow forgot-pass">
      <div class="container">
        <div class="row">
          <div class="forgot-pass-container">     
            <h1><?php echo $lang['forgotpassheader-txt']; ?></h1>
            <p><?php echo $lang['forgotpasssubheader-txt']; ?></p>
            <form name="forgot-pass-email" action="" method="POST">
              
              <label for=""><?php echo $lang['email']; ?></label>
              <?php if ( $_SESSION["messagePassForgot"] != '' ) { ?>
                      <span id="errMsg" class="error" style="margin-left: 0px;display:block"><?php echo $_SESSION["messagePassForgot"]; ?></span>
              <?php } ?>
              <?php if ( $_SESSION["successPassForgot"] != '' ) { ?>
                    <span id="errMsg" class="error" style="margin-left: 0px;color:green !important; font-weight:bold;display:block"><?php echo $_SESSION["successPassForgot"]; ?></span>
              <?php } ?>
              <input type="email" id="forgot-pass-email" class="input email" name="forgot-pass-email" placeholder="foobar@email.com" required>
              <span id="error-email-login" class="error"><?php echo $lang['valid-email']; ?></span>
              <span id="error-email-login-pattern" class="error"><?php echo $lang['valid-email-pattern']; ?></span>
              <input type="submit" id="forgot-pass-btn" class="form-submit-button forgot-pass-button" name="forgot-pass" value="<?php echo $lang['resetpass']; ?>"/>
              <p class="form-txt"><?php echo $lang['Take me to']; ?> <a href="signup"><?php echo $lang['Sing In']; ?></a></p>
            </form>  
          </div> 
        </div>
      </div>
    </section><!-- .forgot-password -->

  </main>

<?php
include 'footer.php';
?>