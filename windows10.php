<?php
include 'header.php';
?>
  <section id="intro-windows10">
    <div class="container">
      <div class="content">
        <h2><?php echo $lang['Windows 10']; ?></h2>
        <p class="intro-txt"><?php echo $lang['subhead-windows10']; ?></p>
        <div>
          <a href="settings" id="complete-profile-btn" class="intro-submit-button scrollto" style="padding: 15px 75px;"><?php echo $lang['Take Me There']; ?></a>
        </div>
      </div>
    </div>
  </section><!-- #intro -->

  <main id="main">
  <section id="windows10info1">
    <div class="container">
      <h3><?php echo $lang['Experience The Wonders of Windows 10 Education']; ?></h3>
      <div class="row windows10column">
        <div class="col-lg-6">
          <img class="img-left" style="width: 100%;" src="assets/img/windows10-imgleft.png" alt="" />
        </div>
        <div class="col-lg-6">
          <p class="normal-txt"><?php echo $lang['windows10-writeup']; ?></p>
        </div>
      </div>

      <div class="row windows10column2">
        <div class="col-lg-3">
          <img class="" src="assets/img/windows10-imgbtm1.png" alt="" />
          <h5><?php echo $lang['Familiar Features']; ?></h5>
          <p class="small-txt"><?php echo $lang['familiar-features-writeup']; ?></p>
        </div>
        <div class="col-lg-3">
          <img class="" src="assets/img/windows10-imgbtm2.png" alt="" />
          <h5><?php echo $lang['Verified for Security']; ?></h5>
          <p class="small-txt"><?php echo $lang['verified-security-writeup']; ?></p>
        </div>
        <div class="col-lg-3">
          <img class="" src="assets/img/windows10-imgbtm3.png" alt="" />
          <h5><?php echo $lang['Great Perfomance']; ?></h5>
          <p class="small-txt"><?php echo $lang['great-perfomance-writeup']; ?></p>
        </div>
        <div class="col-lg-3">
          <img class="" src="assets/img/windows10-imgbtm4.png" alt="" />
          <h5><?php echo $lang['Easy Setup']; ?></h5>
          <p class="small-txt"><?php echo $lang['easy-setup-writeup']; ?></p>
        </div>
      </div>
      <div class="btn-windows10">
        <a href="settings" id="complete-profile-btn"><?php echo $lang['CLAIM YOUR ACCESS']; ?></a>
      </div>
    </div>
  </section><!-- #howtoredeem -->

  <section id="knowmore">
    <div class="container">
      <h1><?php echo $lang['knowmore']; ?></h1>
      <p><?php echo $lang['knowmore-txt']; ?></p>
      <div>
          <a href="institution-list" class="form-submit-button scrollto" style="padding: 15px 75px;background:#F2BF02;"><?php echo $lang['ASK NOW!']; ?></a>
      </div>
    </div>
  </section><!-- #howtoredeem -->

  <div id="complete-profile" class="modal">
    <h3><?php echo $lang['titlecompleteprofile']; ?></h3>
    <p><?php echo $lang['completeprofilemsg']; ?></p>
    <a href="#" id="proceed-to-settings" class="button" rel="modal:close">OK</a>
  </div>

  </main>

<?php
include 'footer.php';
?>