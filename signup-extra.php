<?php
include 'header.php';
include 'app/usercontroller.php';
?>
  
  <main id="main">

    <!--==========================
      SIGN UP Extra Section
    ============================-->
    <section id="signup" class="wow signup-extra">
      <div class="container">
        <div class="row">     
            <div class="tabBox">
              <h3><?php echo $lang['Complete These Fields']; ?></h3>
              <div class="tabContainer" style="padding:30px 35px 35px 35px">
                <div id="signupform" class="tabContent" >
                  <form name="signupform-extra" action="" method="POST">
                    <label class="nric-label" for="ic-number1"><?php echo $lang['nric']; ?></label>
                    <input type="text" id="ic-number1" maxlength="6" class="input icnumber1" name="ic-number1" />
                    <span class="divider-icnumber">-</span>
                    <input type="text" id="ic-number2" maxlength="2" class="input icnumber2" name="ic-number2" />
                    <span class="divider-icnumber">-</span>
                    <input type="text" id="ic-number3" maxlength="4" class="input icnumber3" name="ic-number3" />
                    <p id="error-extra-nric" class="error"><?php echo $lang['valid-ic']; ?></p>
                    
                    <label class="phone-label" for="phone"><?php echo $lang['phone']; ?></label>
                    <span class="prefix-phone">+60</span><input type="tel" id="extra-phone" class="input phone" name="phone" />
                    <p id="error-extra-phone" class="error"><?php echo $lang['valid-phone']; ?></p>
                    <br clear="both">
                    <input type="submit" id="submit-extra" class="form-submit-button" name="submit-extra" value="<?php echo $lang['continue']; ?>" />
                  </form>
                </div>   
              </div>
            </div> 
        </div>

      </div>
    </section><!-- #signup -->

  </main>

<?php
include 'footer.php';
?>