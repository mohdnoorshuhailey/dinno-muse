<?php
include 'app/sessions.php';
//var_dump($_SESSION["alldata"]);
//var_dump($_SESSION["access_token"]);
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>MUSE - <?php echo $currentTitle; ?></title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="keywords">
  <meta content="" name="description">

  <!-- Favicons -->
  <link href="#" rel="icon">
  <link href="#" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">

  <!-- Bootstrap CSS File -->
  <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Libraries CSS Files -->
  <link href="lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="lib/animate/animate.min.css" rel="stylesheet">
  <link href="lib/ionicons/css/ionicons.min.css" rel="stylesheet">
  <link href="lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
  <link href="lib/magnific-popup/magnific-popup.css" rel="stylesheet">

  <!-- Main Stylesheet File -->
  <link href="assets/css/style.css<?php if ( MODE_ENV == 'Development') echo '?v='.time(); ?>" rel="stylesheet">

  <!-- jQuery Modal -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css" />

  <!-- Slick Carousel -->
  <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
</head>

<body id="body">
  <!--==========================
    Header
  ============================-->
  <header id="header">
    <div class="container">

      <div id="logo" class="pull-left">
        <a href="<?php echo BASE_URL; ?>"><img src="assets/img/logo-muse-new.png" alt="" title="" /></a>
      </div>

      <nav id="nav-menu-container">
        <ul class="nav-menu">
          <li class="menu-active"><a href="<?php echo BASE_URL; ?>"><?php echo $lang['home']; ?></a></li>
          <li class="menu-has-children"><a href="#"><?php echo $lang['software']; ?></a>
            <ul>
              <li><a href="office365"><?php echo $lang['office365']; ?></a></li>
              <li><a href="imagine"><?php echo $lang['imagine']; ?></a></li>
            </ul>
          </li>
          <li class="menu-has-children"><a href="#"><?php echo $lang['freecourses']; ?></a>
            <ul>
              <li><a href="imagine-academy"><?php echo $lang['imagineacademy']; ?></a></li>
            </ul>
          </li>
          <!--<li><a href="windows10"><?php echo $lang['windows']; ?></a></li>-->
          <li><a href="news-events"><?php echo $lang['news']; ?></a></li>
        </ul>
        <ul class="nav-menu nav-menu-right">
          <?php if ( $_SESSION["allowedlogin"] == TRUE ) { ?>
            <li><div id="circle-head"><?php echo $_SESSION["userdata"]['fullName'][0]; ?></div></li>
            <li class="menu-has-children profile"><a href="#">Hi, <?php echo $_SESSION["userdata"]['fullName'];?> !</a>
              <ul>
                <li><a href="settings"><img class="img-icon" src="assets/img/settings.png" alt=""/><?php echo $lang['settings']; ?></a></li>
                <li><a href="logout"><img class="img-icon" src="assets/img/logout.png" alt=""/><?php echo $lang['logout']; ?></a></li>
              </ul>
            </li>
          <?php } else { ?>
            <li class="text-underline"><a href="login"><?php echo $lang['login']; ?></a></li>
            <li class="button-header"><a href="signup"><?php echo $lang['freesignup']; ?></a></li>
          <?php } ?>
            <li>
            <div class="switch switch-yellow">
              <input type="radio" class="switch-input <?php if ($_SESSION['lang'] == 'en') echo 'choose'; ?>" name="lang-selection" value="en" id="lang-en" <?php if ($_SESSION['lang'] == 'en') echo 'checked'; ?>>
              <label for="lang-en" class="switch-label switch-label-off">EN</label>
              <input type="radio" class="switch-input <?php if ($_SESSION['lang'] == 'bm') echo 'choose'; ?>" name="lang-selection" value="bm" id="lang-bm" <?php if ($_SESSION['lang'] == 'bm') echo 'checked'; ?>>
              <label for="lang-bm" class="switch-label switch-label-on">BM</label>
              <span class="switch-selection"></span>
            </div>

          </li>
        </ul>
      </nav><!-- #nav-menu-container -->
    </div>
  </header><!-- #header -->
