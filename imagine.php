<?php
include 'header.php';
?>
  <section id="intro">
    <div class="container">
      <div class="content">
        <h2><?php echo $lang['Microsoft Imagine']; ?></h2>
        <p class="intro-txt"><?php echo $lang['subhead-imagine']; ?></p>
        <div>
        <?php if ( $_SESSION["userdata"]['schoolEmail'] == '' ) { ?>
          <a href="settings" class="intro-submit-button scrollto" style="padding: 15px 75px;"><?php echo $lang['Take Me There']; ?></a>
        <?php } else if ( $_SESSION["userdata"]['isSchoolEmailActivated'] == false ) { ?>
          <a href="settings" class="intro-submit-button scrollto" style="padding: 15px 75px;"><?php echo $lang['Take Me There']; ?></a>
        <?php } else {?>
          <a href="https://imagine.microsoft.com/en-us" target="_blank" class="intro-submit-button scrollto" style="padding: 15px 75px;"><?php echo $lang['Take Me There']; ?></a>
        <?php } ?>
        </div>
      </div>
    </div>
  </section><!-- #intro -->

  <main id="main">
  <section id="writeup">
    <div class="container">
      <div class="content">
        <h3><?php echo $lang['writeup-head']; ?></h3>
        <p class="normal-txt"><?php echo $lang['writeup-content']; ?></p></div>
    </div>
  </section><!-- #writeup -->

  <section id="howtoredeem">
    <div class="container">
    <h3><?php echo $lang['How To Redeem']; ?></h3>
      <div class="row">
        <div class="col-lg-4">
          <img src="assets/img/imagine-verify.png" width="180" alt="" />
          <h3><?php echo $lang['Verify University Partner']; ?></h3>
          <p class="normal-txt"><?php echo $lang['verify-partner']; ?></p>
        </div>
        <div class="col-lg-4">
          <img src="assets/img/imagine-confirm.png" width="180" alt="" />
          <h3><?php echo $lang['Email Confirmation']; ?></h3>
          <p class="normal-txt"><?php echo $lang['email-confirm']; ?></p>
        </div>
        <div class="col-lg-4">
          <img src="assets/img/imagine-learning.png" width="180" alt="" />
          <h3><?php echo $lang['Start Learning']; ?></h3>
          <p class="normal-txt"><?php echo $lang['start-learning']; ?></p>
        </div>
      </div>
    </div>
  </section><!-- #howtoredeem -->
  
  <section id="products">
    <div class="container">
    <h3><?php echo $lang['product-title']; ?></h3>
      <div class="row tabContainer">
        <div class="col-lg-4">
          <img src="assets/img/imagine-operating.png" width="230" alt="" />
          <h5><?php echo $lang['Operating Systems']; ?></h5>
        </div>
        <div class="col-lg-8 products-collection">
          <p><?php echo $lang['Product include:'];?></p>
          <ul class="product-list">
            <li><img src="assets/img/imagine-es.jpg" width="60" alt=""><br>Exchange Server Enterprise</li>
            <li><img src="assets/img/imagine-ws.jpg" width="60" alt=""><br>Windows Server Data Center</li>
            <li><img src="assets/img/imagine-ws.jpg" width="60" alt=""><br>Windows Server Standard</li>
            <li><img src="assets/img/imagine-es.jpg" width="60" alt=""><br>Exchange Server Standard</li>
          </ul>
        </div>
      </div>
      <div class="row tabContainer">
        <div class="col-lg-4">
          <img src="assets/img/imagine-dd.jpg" width="230" alt="" />
          <h5><?php echo $lang['Developer & Design Tools']; ?></h5>
        </div>
        <div class="col-lg-8 products-collection">
          <p><?php echo $lang['Product include:'];?></p>
          <ul class="product-list">
            <li><img src="assets/img/imagine-access.jpg" width="60" alt=""><br>Access</li>
            <li><img src="assets/img/imagine-visual.jpg" width="60" alt=""><br>Visual Basic</li>
            <li><img src="assets/img/imagine-visual.jpg" width="60" alt=""><br>Visual Studio Enterprise</li>
            <li><img src="assets/img/imagine-visual.jpg" width="60" alt=""><br>Visual Studio Professional</li>
            <li><img src="assets/img/imagine-visual.jpg" width="60" alt=""><br>Visual Studio Foundation Server</li>
          </ul>
        </div>
      </div>
      <div class="row tabContainer">
        <div class="col-lg-4">
          <img src="assets/img/imagine-app.jpg" width="230" alt="" />
          <h5><?php echo $lang['Applications']; ?></h5>
        </div>
        <div class="col-lg-8 products-collection">
          <p><?php echo $lang['Product include:'];?></p>
          <ul class="product-list">
            <li><img src="assets/img/imagine-visio.jpg" width="60" alt=""><br>Visio Standard</li>
            <li><img src="assets/img/imagine-visio.jpg" width="60" alt=""><br>Visio Professional</li>
            <li><img src="assets/img/imagine-project.jpg" width="60" alt=""><br>Project Standard</li>
            <li><img src="assets/img/imagine-project.jpg" width="60" alt=""><br>Project Professional</li>
            <li><img src="assets/img/imagine-note.jpg" width="60" alt=""><br>OneNote</li>
            <li><img src="assets/img/imagine-info.jpg" width="60" alt=""><br>InfoPath</li>
            <li><img src="assets/img/imagine-sc.jpg" width="60" alt=""><br>System Center Configuration Manager</li>
            <li><img src="assets/img/imagine-mf.jpg" width="60" alt=""><br>Forefront Protection</li>
          </ul>
        </div>
      </div>
      <div class="row tabContainer">
        <div class="col-lg-4">
          <img src="assets/img/imagine-server.jpg" width="230" alt="" />
          <h5><?php echo $lang['Servers']; ?></h5>
        </div>
        <div class="col-lg-8 products-collection">
          <p><?php echo $lang['Product include:'];?></p>
          <ul class="product-list">
            <li><img src="assets/img/imagine-sp.jpg" width="60" alt=""><br>SharePoint Server</li>
            <li><img src="assets/img/imagine-spd.jpg" width="60" alt=""><br>SharePoint Designer</li>
          </ul>
        </div>
      </div>

    </div>
  </section><!-- #products -->

  <section id="knowmore">
    <div class="container">
      <h1><?php echo $lang['knowmore']; ?></h1>
      <p><?php echo $lang['knowmore-txt']; ?></p>
      <div>
          <a href="institution-list" class="form-submit-button scrollto" style="padding: 15px 75px;background:#F2BF02;"><?php echo $lang['ASK NOW!']; ?></a>
      </div>
    </div>
  </section><!-- #howtoredeem -->

  </main>

<?php
include 'footer.php';
?>