<?php
include 'header.php';
include 'app/usercontroller.php';

if ( $_SESSION["userdata"]['fullName'] != NULL 
  AND $_SESSION["userdata"]['dateOfBirth'] != NULL 
  AND $_SESSION["userdata"]['icpassportNo'] != NULL
  AND $_SESSION["userdata"]['mobile'] != NULL
  AND $_SESSION["userdata"]['gender'] != NULL
  AND $_SESSION["userdata"]['insitutionName'] != NULL
  AND $_SESSION["userdata"]['email'] != NULL
  AND $_SESSION["userdata"]['schoolEmail'] != NULL
  ) {
    $displayNotice = FALSE;
  } else {
    $displayNotice = TRUE;
  }

  if ( $_SESSION["userdata"]['schoolEmail'] == ''  ) {
    $displayVerify = TRUE;
  } else if ( $_SESSION["userdata"]['isSchoolEmailActivated'] == false ) {
    $displayVerifyEmail = TRUE;
  } else {
    $displayVerify = FALSE;
    $displayVerifyEmail = FALSE;
  }


  if ( $_SESSION["allowedlogin"] === false ) {
    header('Location: login');
    exit;
  }
?>

  <main id="main">

    <!--==========================
      Settings Section
    ============================-->
    <section id="settings" class="wow user-profile">
    <?php if ($displayNotice === TRUE ) { ?><div class="annouce-txt"><?php echo $lang['annouce-txt']; ?></div> <?php } ?>
      <div class="container">
        <div class="row">     
          <div class="col-lg-3 profile-left">
            <div class="profile-info">
              <div id="circle"><?php echo $_SESSION["userdata"]['fullName'][0]; ?></div>
              <div class="profile-name"><?php echo $_SESSION["userdata"]['fullName']; ?></div>
            </div>
            <div class="profile-link">
              <ul>
                <li><a id="personal-info-btn" href="#" class="scrollto"><img src="assets/img/personal-info.png" alt="" align="middle" style="width: 20px;margin-right: 12px;"/><?php echo $lang['Personal Information']; ?></a></li>
                <li><a id="account-info-btn" href="#" class="scrollto"><img src="assets/img/account-info.png" alt="" align="middle" style="width: 20px;margin-right: 12px;"/><?php echo $lang['Account Information']; ?></a></li>
                <li><a id="credentials-btn" href="#" class="scrollto"><img src="assets/img/passwd.png" alt="" align="middle" style="width: 20px;margin-right: 12px;"/><?php echo $lang['Credentials']; ?></a></li>
              </ul>
            </div>
          </div>
          <div class="col-lg-9 profile-right">
            <div id="personal-info" class="personal-info tabContainer">
              <h5><?php echo $lang['Personal Information']; ?></h5>
              <form class="personalinfo" name="personalinfo" action="" method="POST">
                <?php if ( $_SESSION["message"] != '' ) { ?>
                      <p id="errMsg" class="error" style="margin-left: 0px;display:block"><?php echo $_SESSION["message"]; ?></p>
                <?php } ?>
                <?php if ( $_SESSION["success"] != '' ) { ?>
                      <p id="errMsg" class="error" style="margin-left: 0px;color:green !important; font-weight:bold;display:block"><?php echo $_SESSION["success"]; ?></p>
                <?php } ?>
                <label><?php echo $lang['fullname']; ?></label>
                <input type="text" id="fullname" class="input" name="fullName" value="<?php echo $_SESSION["userdata"]['fullName']; ?>" >
                
                <label><?php echo $lang['dob']; ?></label>
                <input type="text" id="dob" class="input" name="dateOfBirth" value="<?php echo date("d-M-Y", strtotime($_SESSION["userdata"]['dateOfBirth'])); ?>" placeholder="14-03-1980">
                
                <label><?php echo $lang['ic-passport']; ?></label>
                <input type="text" id="icnumber" class="input" name="icpassportNo" value="<?php echo $_SESSION["userdata"]['icpassportNo']; ?>">

                <label><?php echo $lang['phone']; ?></label>
                <input type="tel" id="phone-number" class="input" name="mobile" value="<?php echo $_SESSION["userdata"]['mobile']; ?>" placeholder="0123456789" required>
               
                <label><?php echo $lang['gender']; ?></label>
                <input type="radio" class="settings-radio" name="gender" value="Male" <?php if( $_SESSION["userdata"]['gender'] == 'Male' ) echo 'checked'; ?>><label class="radio">Male</label>
                <input type="radio" class="settings-radio" name="gender" value="Female" <?php if( $_SESSION["userdata"]['gender'] == 'Female' ) echo 'checked'; ?>><label class="radio">Female</label>
                <br clear="all"/>
                <label><?php echo $lang['inst-name']; ?></label>
                <select class="insitutionName" id="inst-name" name="insitutionName">
                      <option value=""><?php echo $lang['inst-name']; ?></option>
                      <option value="International Islamic University Malaysia (IIUM)" <?php if ($_SESSION["userdata"]['insitutionName'] == 'International Islamic University Malaysia (IIUM)') echo 'selected'; ?>>International Islamic University Malaysia (IIUM)</option>
                      <option value="Kolej Komuniti Bentong" <?php if ($_SESSION["userdata"]['insitutionName'] == 'Kolej Komuniti Bentong') echo 'selected'; ?>>Kolej Komuniti Bentong</option>
                      <option value="Kolej Komuniti Kuala Langat" <?php if ($_SESSION["userdata"]['insitutionName'] == 'Kolej Komuniti Kuala Langat') echo 'selected'; ?>>Kolej Komuniti Kuala Langat</option>
                      <option value="Kolej Komuniti Selandar" <?php if ($_SESSION["userdata"]['insitutionName'] == 'Kolej Komuniti Selandar') echo 'selected'; ?>>Kolej Komuniti Selandar</option>
                      <option value="Politeknik Kuala Terengganu" <?php if ($_SESSION["userdata"]['insitutionName'] == 'Politeknik Kuala Terengganu') echo 'selected'; ?>>Politeknik Kuala Terengganu</option>
                      <option value="Politeknik Kuching Sarawak" <?php if ($_SESSION["userdata"]['insitutionName'] == 'Politeknik Kuching Sarawak') echo 'selected'; ?>>Politeknik Kuching Sarawak</option>
                      <option value="Politeknik Muadzam Shah" <?php if ($_SESSION["userdata"]['insitutionName'] == 'Politeknik Muadzam Shah') echo 'selected'; ?>>Politeknik Muadzam Shah</option>
                      <option value="Politeknik Seberang Perai" <?php if ($_SESSION["userdata"]['insitutionName'] == 'Politeknik Seberang Perai') echo 'selected'; ?>>Politeknik Seberang Perai</option>
                      <option value="Politeknik Sultan Abdul Halim Mu'adzam Shah" <?php if ($_SESSION["userdata"]['insitutionName'] == "Politeknik Sultan Abdul Halim Mu'adzam Shah") echo 'selected'; ?>>Politeknik Sultan Abdul Halim Mu'adzam Shah</option>
                      <option value="Politeknik Sultan Haji Ahmad Shah" <?php if ($_SESSION["userdata"]['insitutionName'] == 'Politeknik Sultan Haji Ahmad Shah') echo 'selected'; ?>>Politeknik Sultan Haji Ahmad Shah</option>
                      <option value="Politeknik Sultan Idris Shah" <?php if ($_SESSION["userdata"]['insitutionName'] == 'Politeknik Sultan Idris Shah') echo 'selected'; ?>>Politeknik Sultan Idris Shah</option>
                      <option value="Politeknik Sultan Mizan Zainal Abidin" <?php if ($_SESSION["userdata"]['insitutionName'] == 'Politeknik Sultan Mizan Zainal Abidin') echo 'selected'; ?>>Politeknik Sultan Mizan Zainal Abidin</option>
                      <option value="Politeknik Ungku Omar" <?php if ($_SESSION["userdata"]['insitutionName'] == 'Politeknik Ungku Omar') echo 'selected'; ?>>Politeknik Ungku Omar</option>
                      <option value="Universiti Kebangsaan Malaysia (UKM)" <?php if ($_SESSION["userdata"]['insitutionName'] == 'Universiti Kebangsaan Malaysia (UKM)') echo 'selected'; ?>>Universiti Kebangsaan Malaysia (UKM)</option>
                      <option value="Universiti Malaya (UM)" <?php if ($_SESSION["userdata"]['insitutionName'] == 'Universiti Malaya (UM)') echo 'selected'; ?>>Universiti Malaya (UM)</option>
                      <option value="Universiti Malaysia Kelantan (UMK)" <?php if ($_SESSION["userdata"]['insitutionName'] == 'Universiti Malaysia Kelantan (UMK)') echo 'selected'; ?>>Universiti Malaysia Kelantan (UMK)</option>
                      <option value="Universiti Malaysia Pahang (UMP)" <?php if ($_SESSION["userdata"]['insitutionName'] == 'Universiti Malaysia Pahang (UMP)') echo 'selected'; ?>>Universiti Malaysia Pahang (UMP)</option>
                      <option value="Universiti Malaysia Perlis(UNIMAP)" <?php if ($_SESSION["userdata"]['insitutionName'] == 'Universiti Malaysia Perlis(UNIMAP)') echo 'selected'; ?>>Universiti Malaysia Perlis(UNIMAP)</option>
                      <option value="Universiti Malaysia Sabah (UMS)" <?php if ($_SESSION["userdata"]['insitutionName'] == 'Universiti Malaysia Sabah (UMS)') echo 'selected'; ?>>Universiti Malaysia Sabah (UMS)</option>
                      <option value="Universiti Malaysia Sarawak (UNIMAS)" <?php if ($_SESSION["userdata"]['insitutionName'] == 'Universiti Malaysia Sarawak (UNIMAS)') echo 'selected'; ?>>Universiti Malaysia Sarawak (UNIMAS)</option>
                      <option value="Universiti Malaysia Terengganu (UMT)" <?php if ($_SESSION["userdata"]['insitutionName'] == 'Universiti Malaysia Terengganu (UMT)') echo 'selected'; ?>>Universiti Malaysia Terengganu (UMT)</option>
                      <option value="Universiti Pendidikan Sultan Idris (UPSI)" <?php if ($_SESSION["userdata"]['insitutionName'] == 'Universiti Pendidikan Sultan Idris (UPSI)') echo 'selected'; ?>>Universiti Pendidikan Sultan Idris (UPSI)</option>
                      <option value="Universiti Pertahanan Nasional Malaysia (UPNM)" <?php if ($_SESSION["userdata"]['insitutionName'] == 'Universiti Pendidikan Sultan Idris (UPSI)') echo 'selected'; ?>>Universiti Pertahanan Nasional Malaysia (UPNM)</option>
                      <option value="Universiti Putra Malaysia (UPM)" <?php if ($_SESSION["userdata"]['insitutionName'] == 'Universiti Putra Malaysia (UPM)') echo 'selected'; ?>> Universiti Putra Malaysia (UPM)</option>
                      <option value="Universiti Sains Islam Malaysia (USIM)" <?php if ($_SESSION["userdata"]['insitutionName'] == 'Universiti Sains Islam Malaysia (USIM)') echo 'selected'; ?>>Universiti Sains Islam Malaysia (USIM)</option>
                      <option value="Universiti Sains Malaysia (USM)" <?php if ($_SESSION["userdata"]['insitutionName'] == 'Universiti Sains Malaysia (USM)') echo 'selected'; ?>>Universiti Sains Malaysia (USM)</option>
                      <option value="Universiti Sultan Zainal Abidin (UniSZA)" <?php if ($_SESSION["userdata"]['insitutionName'] == 'Universiti Sultan Zainal Abidin (UniSZA)') echo 'selected'; ?>>Universiti Sultan Zainal Abidin (UniSZA)</option>
                      <option value="Universiti Teknikal Malaysia Melaka" <?php if ($_SESSION["userdata"]['insitutionName'] == 'Universiti Teknikal Malaysia Melaka') echo 'selected'; ?>>Universiti Teknikal Malaysia Melaka</option>
                      <option value="Universiti Teknologi Malaysia (UTM)" <?php if ($_SESSION["userdata"]['insitutionName'] == 'Universiti Teknologi Malaysia (UTM)') echo 'selected'; ?>>Universiti Teknologi Malaysia (UTM)</option>
                      <option value="Universiti Teknologi Mara (UiTM)" <?php if ($_SESSION["userdata"]['insitutionName'] == 'Universiti Teknologi Mara (UiTM)') echo 'selected'; ?>>Universiti Teknologi Mara (UiTM)</option>
                      <option value="Universiti Tun Hussein Onn Malaysia (UTHM)" <?php if ($_SESSION["userdata"]['insitutionName'] == 'Universiti Tun Hussein Onn Malaysia (UTHM)') echo 'selected'; ?>>Universiti Tun Hussein Onn Malaysia (UTHM)</option>
                      <option value="Universiti Utara Malaysia (UUM)" <?php if ($_SESSION["userdata"]['insitutionName'] == 'Universiti Utara Malaysia (UUM)') echo 'selected'; ?>>Universiti Utara Malaysia (UUM)</option>  
                    </select>
                <div style="clear: both;"></div>
                <input type="submit" id="submit" class="settings-submit-button" name="update-profile" value="SAVE" />
              </form>
            </div>
            <div id="account-info" class="account-info tabContainer">
              <h5><?php echo $lang['Account Information']; ?></h5>
              <?php if ($displayVerify === TRUE ) { ?><div id="errMsg" class="error" style="margin-left: 0px;display:block;float: none;"><?php echo $lang['verify-email']; ?></div><?php } ?>
              <?php if ($displayVerifyEmail === TRUE ) { ?><div id="errMsg" class="error" style="margin-left: 0px;display:block;float: none;"><?php echo $lang['verify-email-msg']; ?></div><?php } ?>
              <form class="accountinfo" name="accountinfo" action="" method="POST">
                <?php if ( $_SESSION["messageAcc"] != '' ) { ?>
                      <p id="errMsg" class="error" style="margin-left: 0px;display:block"><?php echo $_SESSION["messageAcc"]; ?></p>
                <?php } ?>

                <label><?php echo $lang['reg-email']; ?></label>
                <input type="text" id="reg-email" class="input" name="email" value="<?php echo $_SESSION["userdata"]['email']; ?>" disabled>
                <label><?php echo $lang['inst-email']; ?></label>
                <input type="text" id="intitution-email" class="input" name="schoolEmail" value="<?php echo $_SESSION["userdata"]['schoolEmail']; ?>" required>
              
                <div style="clear: both;"></div>
                <input type="submit" id="submitaccountinfo" class="settings-submit-button" name="update-accountinfo" value="SAVE" />
              </form>
            </div>
            <div id="credentials" class="credentials tabContainer">
              <h5><?php echo $lang['Credentials']; ?></h5>
              <form class="credentialinfo" name="credentialinfo" action="" method="POST">
                <?php if ( $_SESSION["messagePass"] != '' ) { ?>
                      <p id="errMsg" class="error" style="margin-left: 0px;display:block"><?php echo $_SESSION["messagePass"]; ?></p>
                <?php } ?>
                <?php if ( $_SESSION["successPass"] != '' ) { ?>
                      <p id="errMsg" class="error" style="margin-left: 0px;color:green !important; font-weight:bold;display:block"><?php echo $_SESSION["successPass"]; ?></p>
                <?php } ?>
                <label><?php echo $lang['password']; ?></label>
                <input type="password" id="current-pass" class="input" name="current-pass" placeholder="<?php echo $lang['current-pass']; ?>"/>

                <input type="password" id="new-pass" class="input" name="new-pass" placeholder="<?php echo $lang['new-pass']; ?>"/>

                <input type="password" id="verify-pass" class="input" name="new-pass-repeat" placeholder="<?php echo $lang['verify-pass']; ?>"/>

                <div style="clear: both;"></div>
                <input type="submit" id="submitpassinfo" class="settings-submit-button" name="update-passinfo" value="SAVE" />
              </form>
            </div>
          </div>
        </div>

      </div>
    </section><!-- #settings -->

  </main>
  <div id="complete-schoolemail" class="modal">
    <p style="color:green !important; font-weight:bold;"><?php echo $_SESSION["successAcc"]; ?></p>
    <a href="#" class="button" rel="modal:close">OK</a>
  </div>

<?php
include 'footer.php';
?>