<?php

include 'app/sessions.php';

session_unset();    
session_destroy();
header("Location: login");
exit;